<?php

namespace AdminBundle\Form;

use AppBundle\Entity\Community;
use AppBundle\Entity\Location;
use AppBundle\Entity\Plant;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;


class AddPlantType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('binomialName', TextType::class)
            ->add('imageField', FileType::class, [
                "required" => false
            ])
            ->add('description', TextareaType::class)
            ->add('height', NumberType::class)
            ->add('community', EntityType::class,[
                "class" => Community::class,
                "choice_label" => 'name',
                "expanded" => true,
                "multiple" => false,
                "required" => false,
                "placeholder" => "Žádné"
            ])
            ->add('locations', CollectionType::class,[
                "allow_add" => true,
                "allow_delete" => true,
                "entry_type" => EntityType::class,
                "label" => false,
                "entry_options"  => array(
                    "class" => Location::class,
                    'query_builder' => function(EntityRepository $repository) {
                        return $repository->createQueryBuilder('l')->orderBy('l.name', 'ASC');
                    },
                    "label" => false,
                ),
            ])
            ->add('submit', SubmitType::class, [
            ])
            ->getForm();
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'request' => null,
            'entity_manager' => null,
        ));
    }
}
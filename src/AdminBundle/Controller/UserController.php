<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserController extends Controller
{
    /**
     * @Route("/admin/users", name="admin_users")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository('AppBundle:User')->findAll();

        return $this->render('admin/users.html.twig',[
            "users" => $users
        ]);
    }

    /**
     * @Route("/admin/users/{user}/promote", name="admin_users_promote")
     * @param Request $request
     * @return Response
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function userPromoteAction(Request $request, User $user)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user->addRole('ROLE_ADMIN');

        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash('success', 'Uživateli '.$user->getUsername().' byla úspěšně přidána role administratátora.');

        return $this->redirectToRoute('admin_users');
    }

    /**
     * @Route("/admin/users/{user}/demote", name="admin_users_demote")
     * @param Request $request
     * @return Response
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function userDemoteAction(Request $request, User $user)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user->removeRole('ROLE_ADMIN');

        $entityManager->persist($user);
        $entityManager->flush();

        $this->addFlash('success', 'Uživateli '.$user->getUsername().' byla úspěšně odebrána role administrátora.');

        return $this->redirectToRoute('admin_users');
    }
}
<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Location;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;


class LocationController extends Controller
{
    /**
     * @Route("/admin/locations", name="admin_locations")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $countries = $entityManager->getRepository('AppBundle:Country')->findBy([], ['name' => 'ASC']);

        return $this->render('admin/locations.html.twig', [
            'countries' => $countries
        ]);
    }

    /**
     * @Route("/admin/locations/save-name", name="admin_locations_save")
     * @param Request $request
     * @return Response
     */
    public function saveLocationAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $locationId = $request->get('location');
        $newName = $request->get('newName');
        $country = $entityManager->getRepository('AppBundle:Country')->find($request->get('country'));

        if ($locationId == 'new'){
            $location = new Location();
            $location->setName($newName);
            $location->setCountry($country);

            $this->addFlash('success', 'Lokace '.$location->getName().' byla úspěšně přidána do databáze.');
        }
        else{
            $location = $entityManager->getRepository('AppBundle:Location')->find($locationId);
            $location->setName($newName);
        }

        $entityManager->persist($location);
        $entityManager->flush();

        $response = new Response($newName);

        return $response;
    }

    /**
     * @Route("/admin/location/{location}/delete", name="admin_locations_delete")
     * @param Request $request
     * @return Response
     */
    public function deleteLocationAction(Request $request, Location $location)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($location);
        $entityManager->flush();

        $this->addFlash('success', 'Lokace '.$location->getName().' byla úspěšně odebrána z databáze.');

        return $this->redirectToRoute('admin_locations');
    }
}
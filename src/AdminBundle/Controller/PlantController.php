<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\AddPlantType;
use AdminBundle\Form\EditPlantType;
use AppBundle\Entity\Plant;
use Cocur\Slugify\Slugify;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;


class PlantController extends Controller
{
    /**
     * @Route("/admin/plants", name="admin_plants")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $plants = $entityManager->getRepository('AppBundle:Plant')->findBy([], ["name" => "ASC"]);

        return $this->render('admin/plants.html.twig',[
            "plants" => $plants
        ]);
    }

    /**
     * @Route("/admin/plants/add", name="admin_plants_add")
     * @param Request $request
     * @return Response
     */
    public function addPlantAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $addPlantForm = $this->createForm(AddPlantType::class);
        $addPlantForm->handleRequest($request);

        $locations = $entityManager->getRepository('AppBundle:Location')->findBy([], ['name' => 'ASC']);

        if ($addPlantForm->isSubmitted() && $addPlantForm->isValid()){

            $addPlantFormData = $addPlantForm->getData();
            $name = $addPlantFormData['name'];

            $entityManager->getRepository('AppBundle:Plant')->findBy(['name' => $name]);

            $binomialName = $addPlantFormData['binomialName'];
            $description = $addPlantFormData['description'];
            $height = $addPlantFormData['height'];
            $community = $addPlantFormData['community'];
            $locations = $addPlantFormData['locations'];

            $plant = new Plant();
            $plant->setName($name);
            $plant->setBinomialName($binomialName);
            $plant->setDescription($description);
            $plant->setHeight($height);
            $plant->setCommunity($community);
            $plant->setLocations($locations);

            $slug = new Slugify();
            $slug = $slug->slugify($name);
            $plant->setSlug($slug);

            if ($entityManager->getRepository('AppBundle:Plant')->findBy(['slug' => $slug])){
                $this->addFlash('error', 'Nelze přidat tento záznam. Invazivní rostlina s názvem '.$name.' již v databázi existuje.');
                return $this->redirectToRoute('admin_plants_add');
            }

            if (key_exists('imageField', $addPlantFormData)){
                $image = $addPlantFormData['imageField'];
                if ($image != null){
                    $image->move($this->get('kernel')->getRootDir() . '/../web/images/rostliny/', $slug . '.jpg');
                    $plant->setImage($slug);
                }
            }

            $entityManager->persist($plant);
            $entityManager->flush();

            $this->addFlash('success', 'Invazivní rostlina '.$name.' byla úspěšně přidána do databáze.');

            return $this->redirectToRoute('admin_plants');
        }

        return $this->render('admin/addPlant.html.twig', [
            "addPlantForm" => $addPlantForm->createView(),
            "locations" => $locations
        ]);
    }

    /**
     * @Route("/admin/plants/{slug}/edit", name="admin_plants_edit")
     * @param Request $request
     * @return Response
     */
    public function editPlantAction(Request $request, $slug)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $plant = $entityManager->getRepository('AppBundle:Plant')->findOneBy(['slug' => $slug]);
        $defaultSlug = $slug;

        $editPlantForm = $this->createForm(EditPlantType::class, $plant);
        $editPlantForm->handleRequest($request);

        $locations = $entityManager->getRepository('AppBundle:Location')->findBy([], ['name' => 'ASC']);

        if ($editPlantForm->isSubmitted() && $editPlantForm->isValid()){

            $editPlantFormData = $editPlantForm->getData();
            $name = $editPlantFormData->getName();
            $binomialName = $editPlantFormData->getBinomialName();
            $description = $editPlantFormData->getDescription();
            $height = $editPlantFormData->getHeight();
            $community = $editPlantFormData->getCommunity();
            $locations = $editPlantFormData->getLocations();

            $plant->setName($name);
            $plant->setBinomialName($binomialName);
            $plant->setDescription($description);
            $plant->setHeight($height);
            $plant->setCommunity($community);
            $plant->setLocations($locations);

            $slug = new Slugify();
            $slug = $slug->slugify($name);
            $plant->setSlug($slug);

            if ($entityManager->getRepository('AppBundle:Plant')->findBy(['slug' => $slug]) && $plant->getSlug() != $defaultSlug){
                $this->addFlash('error', 'Tyto úpravy nelze provést. Invazivní rostlina s názvem '.$name.' již v databázi existuje.');
                return $this->redirectToRoute('admin_plants_edit', ['slug' => $defaultSlug]);
            }

            if ($editPlantForm->get('imageField')){
                $image = $editPlantForm->get('imageField')->getData();

                if ($image != null){
                    $image->move($this->get('kernel')->getRootDir() . '/../web/images/rostliny/', $slug . '.jpg');
                    $plant->setImage($slug);
                }
            }

            $entityManager->persist($plant);
            $entityManager->flush();

            return $this->redirectToRoute('admin_plants');
        }

        return $this->render('admin/editPlant.html.twig', [
            "editPlantForm" => $editPlantForm->createView(),
            "locations" => $locations,
            "plant" => $plant
        ]);
    }

    /**
     * @Route("/admin/plants/{slug}/delete-picture", name="admin_plants_delete_picture")
     * @param Request $request
     * @return Response
     */
    public function deletePicturePlantAction(Request $request, $slug)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $plant = $entityManager->getRepository('AppBundle:Plant')->findOneBy(['slug' => $slug]);

        $file_path = $this->get('kernel')->getRootDir() . '/../web/images/rostliny/' . $plant->getImage() . '.jpg';
        if (file_exists($file_path)) {
            unlink($file_path);
        }

        return $this->redirectToRoute('admin_plants_edit', ['slug' => $plant->getSlug()]);
    }

    /**
     * @Route("/admin/plants/{slug}/delete", name="admin_plants_delete")
     * @param Request $request
     * @return Response
     */
    public function deletePlantAction(Request $request, $slug)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();
        $plant = $entityManager->getRepository('AppBundle:Plant')->findOneBy(['slug' => $slug]);

        $entityManager->remove($plant);

        $entityManager->flush();

        return $this->redirectToRoute('admin_plants');
    }
}
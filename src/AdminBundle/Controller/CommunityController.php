<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\AddPlantType;
use AdminBundle\Form\EditPlantType;
use AppBundle\Entity\Community;
use AppBundle\Entity\Plant;
use Cocur\Slugify\Slugify;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;


class CommunityController extends Controller
{
    /**
     * @Route("/admin/community", name="admin_community")
     * @return Response
     */
    public function indexAction()
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $communities = $entityManager->getRepository('AppBundle:Community')->findBy([], ['name' => 'ASC']);
        $unassignedPlants = $entityManager->getRepository('AppBundle:Plant')->findBy(['community' => null], ['name' => 'ASC']);

        return $this->render('admin/community.html.twig',[
            "communities" => $communities,
            "unassignedPlants" => $unassignedPlants
        ]);
    }

    /**
     * @Route("/admin/community/save-name", name="admin_community_save")
     * @param Request $request
     * @return Response
     */
    public function saveCommunityAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $communityId = $request->get('community');
        $newTitle = $request->get('newTitle');

        if ($communityId == 'new'){
            $community = new Community();
            $community->setName($newTitle);

            $this->addFlash('success', 'Společenstvo '.$community->getName().' bylo úspěšně přidáno do databáze.');
        }
        else{
        $community = $entityManager->getRepository('AppBundle:Community')->find($communityId);
        $community->setName($newTitle);
        }

        $entityManager->persist($community);
        $entityManager->flush();

        $response = new Response($newTitle);

        return $response;
    }

    /**
     * @Route("/admin/community/{community}/delete", name="admin_community_delete")
     * @param Community $community
     * @return Response
     */
    public function deleteCommunityAction(Community $community)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($community);
        $entityManager->flush();

        $this->addFlash('success', 'Společenstvo '.$community->getName().' bylo úspěšně odebráno z databáze.');

        return $this->redirectToRoute('admin_community');
    }
}
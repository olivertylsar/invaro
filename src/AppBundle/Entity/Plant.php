<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Plant
 *
 * @ORM\Table(name="plant")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlantRepository")
 */
class Plant
{
    public function __construct()
    {
        $this->locations = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="binomial_name", type="string", length=255)
     */
    private $binomialName;

    /**
     * @var string
     *
     * @ORM\Column(name="height", type="string")
     */
    private $height;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Location", inversedBy="plants")
     * @ORM\JoinTable(name="plant_location")
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $locations;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=256, nullable=true)
     */
    private $image;

    /**
     * @var Community
     * @ORM\ManyToOne(targetEntity="Community", inversedBy="plants")
     * @ORM\JoinColumn(name="community_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $community;

    /**
     * @ORM\OneToMany(targetEntity="GalleryPhoto", mappedBy="plant")
     */
    protected $galleryPhotos;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Plant
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set binomialName
     *
     * @param string $binomialName
     *
     * @return Plant
     */
    public function setBinomialName($binomialName)
    {
        $this->binomialName = $binomialName;

        return $this;
    }

    /**
     * Get binomialName
     *
     * @return string
     */
    public function getBinomialName()
    {
        return $this->binomialName;
    }

    /**
     * Set height
     *
     * @param string $height
     *
     * @return Plant
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Get locations
     *
     * @return ArrayCollection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Set locations
     *
     * @return Plant
     */
    public function setLocations($locations)
    {
        $this->locations = $locations;

        return $this;
    }


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Plant
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Plant
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Plant
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set community
     *
     * @param string $community
     *
     * @return Plant
     */
    public function setCommunity($community)
    {
        $this->community = $community;

        return $this;
    }

    /**
     * Get community
     *
     * @return string
     */
    public function getCommunity()
    {
        return $this->community;
    }

    /**
     * Get galleryPhotos
     *
     * @return ArrayCollection
     */
    public function getGalleryPhotos()
    {
        return $this->galleryPhotos;
    }

    public function removeLocation(Location $location)
    {
        $this->locations->removeElement($location);
    }

    public function addLocation(Location $location)
    {
        if (!$this->locations->contains($location)){
        $this->getLocations()->add($location);
        }
    }
}


<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;


class SearchPlantsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $request = $options['request'];
        $query = $request->get('search');

        $builder
            ->add('query', SearchType::class, [
                "attr" => [
                    "placeholder" => 'Jméno rostliny'
                ],
                "data" => $query
            ])
            ->add('submit', SubmitType::class, [
            ])
            ->getForm();
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'request' => null
        ));
    }
}
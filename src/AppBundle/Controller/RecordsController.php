<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RecordsController extends Controller
{
    /**
     * @Route("/records", name="records")
     */
    public function indexAction(Request $request)
    {
        return $this->render('records.html.twig');
    }
}

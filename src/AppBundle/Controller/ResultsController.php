<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ResultsController extends Controller
{
    /**
     * @Route("/results", name="results")
     */
    public function indexAction(Request $request)
    {
        return $this->render('results.html.twig');
    }
}

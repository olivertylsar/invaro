<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;

class MapController extends Controller
{
    /**
     * @Route("/map", name="map")
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $plants = $entityManager->getRepository('AppBundle:Plant')->findBy([], ["name" => "ASC"]);

        return $this->render('map.html.twig', [
            "plants" => $plants
        ]);
    }

    /**
     * @Route("/map/popup-data", name="map_popup_data")
     * @param Request $request
     * @return Response
     */
    public function popupDataAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $objectId = $request->get('objectId');
        $plant = $entityManager->getRepository('AppBundle:Plant')->find($objectId);
        if ($plant){
            $data = array(
                'name' => $plant->getName(),
                'binomialName' => $plant->getBinomialName(),
                'link' => $this->generateUrl('plants_detail', ['slug' => $plant->getSlug()]),
                'image' => $plant->getImage(),
                'ok' => true
                );
        }
        else{
            $data = array(
                'ok' => false
            );
        }

        return new JSONResponse ($data);

    }
}

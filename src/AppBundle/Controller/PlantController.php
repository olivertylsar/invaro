<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Form\SearchPlantsType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Plant;

class PlantController extends Controller
{
    /**
     * @Route("/plants", name="plants")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $searchForm = $this->createForm(SearchPlantsType::class, null, ['request' => $request]);
        $searchForm->handleRequest($request);

        if($request->get('search')){
            $query = $request->get('search');
        }
        else{
            $query = '';
        }

        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $data = $searchForm->getData();
            $query = $data['query'];
            return $this->redirect($this->get('router')->generate('plants', array('search' => $query)));
        }

        $plants = $entityManager->getRepository('AppBundle:Plant')->findPlantsByQuery($query);

        return $this->render('plants.html.twig', [
            "plants" => $plants,
            "searchForm" => $searchForm->createView(),
            "query" => $query
        ]);
    }

    /**
     * @Route("/plants/{slug}", name="plants_detail")
     * @param Plant $plant
     * @return Response
     */
    public function plantsDetailAction(Plant $plant)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getDoctrine()->getManager();

        $plants = $entityManager->getRepository('AppBundle:Plant')->findBy([], ["name" => "ASC"]);
        $countries = $entityManager->getRepository('AppBundle:Country')->findBy([], ["name" => "ASC"]);
        $gallery = $entityManager->getRepository('AppBundle:GalleryPhoto')->findBy(['plant' => $plant]);

        return $this->render('plantDetail.html.twig', [
            "plant" => $plant,
            "plants" => $plants,
            "countries" => $countries,
            "gallery" => $gallery
        ]);
    }
}
